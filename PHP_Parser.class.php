<?php
/* PHP Parser v1.0
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * This class will parse PHP code into a structured list of typed objects.
 *
 * To Do: 
 * Capture "extends" info in "class extends foo"
 */

namespace PHP_Code_Tools;

class PHP_Parser
{
	public $_src;
	public $_code;
	public $_pos;
	public $_end;
	
	public $Results;
	
	private static $control_structures_with_criteria = array("if","else if","elseif","foreach","for","while","switch");
	
	public function __construct($file_or_code, $type = null)
	{
		// Allow user to explicitly say whether it's a "file" or "code"
		if($type !== null)
		{
			$type = strtolower(trim($type));
			if(($type != "file") && ($type != "code"))
			{
				throw new \Exception("Second parameter must be either null, 'file', or 'code'");
			}
		}
		
		// If we're working with a file or we've auto-detected it's a file...
		if(($type == "file") || (($type === null) && (strpos($file_or_code,'<?') === false) && file_exists($file_or_code)))
		{
			// Replace the first parameter with the file contents
			$file_or_code = file_get_contents($file_or_code);
		}
		
		// Prepare the code
		$this->_src = $file_or_code;
		$this->_code = strtolower($this->_src);
		$this->_pos = -1;
		$this->_end = strlen($this->_src) - 1;
		
		// Now parse it!
		$this->Results = new PHP_Document();
		$this->_Parse($this->Results);
		
		// We don't need a copy of the lower-cased code anymore
		$this->_code = null;
		
		// Fill in the contents
		foreach($this->Results->Parts as $idx => $Part)
		{
			$Part->Fill($this->_src);
		}
	}
	
	private function _Parse(PHP_Code_Container $owner)
	{
		// Debug flag?
		$_debug = false; // (($this->_pos >= 550) && ($this->_pos <= 700));
		
		// Offsets
		$_non_codeblock_start = 0;
		$_php_statement_start = 0;
		
		// States / Flags
		$_in_php_section = !($owner instanceof PHP_Document);
		$_in_php_statement = false;
		$_in_PHP_Document = ($owner instanceof PHP_Document);
		$_in_php_class = ($owner instanceof PHP_Class);
		$_in_switch = (($owner instanceof PHP_Control_Block) && ($owner->Type == "switch"));
		$_in_switch_case = (($owner instanceof PHP_Control_Block) && ($owner->Type == "case"));
		
		// For storing any modifiers for class properties/methods
		$modifiers = array(); 
		$_modifier_start = null;
		
		// Holder for current part
		$_currentPart = null;
		
		// Start at the ground level
		$_php_level = 0;

		// Control structures
		$_ifs = array();
		$_switches = array();
		$_cases = array();
		$_whiles = array();
		$_current_control_structure = null;

		// Debugging
		// if($_debug) { echo "{$this->_pos} :: Start of _Parse(), \$_in_php_section = " . var_export($_in_php_section,true) . ", \$owner->ID = " . $owner->ID . "\n"; }
		
		// Parse code into parts
		while($this->_pos < $this->_end)
		{
			// Get next 2 chars
			$this->_pos++;
			$char = $this->_code[$this->_pos];
			if(($this->_pos + 1) < $this->_end)
			{
				$nextchar = $this->_code[$this->_pos + 1];
			}
			else
			{
				// echo "REACHED END!!!\n";
				// echo "{$this->_pos} :: Start of _Parse(), \$_in_php_section = " . var_export($_in_php_section,true) . ", \$owner->ID = " . $owner->ID . "\n";
				$nextchar = null;
			}

			// $_debug = (($this->_pos >= 10) && ($this->_pos <= 150));

			// Debugging
			if($_debug) { echo "{$this->_pos} :: " . $this->show_char_context() . ", \$_in_php_section = " . var_export($_in_php_section,true) . "\n"; }
			
			// Are we inside a code block?
			if(!$_in_php_section)
			{
				// We're NOT in a PHP code block
		
				// Look for start of PHP code block
				if(($char == '<') && ($nextchar == '?'))
				{
					// We're entering PHP, so add any non-codeblock contents we've accumulated so far
					if($this->_pos > $_non_codeblock_start)
					{
						// ###
						// PHP Non-Code Block (e.g. HTML)
						// ###
						$_currentPart = new PHP_NonCodeBlock($_non_codeblock_start, $this->_pos);
						$owner->Parts[] = $_currentPart;
					}
					
					// Start of a block - see if its <? or <?php and skip ahead chars
					if(substr($this->_code,$this->_pos,5) == '<?php')
					{
						if($_debug) { echo "{$this->_pos} :: Entering PHP block and advancing position by 5 characters...\n"; }
						$this->_pos += 5;
					}
					else
					{
						if($_debug) { echo "{$this->_pos} :: Entering PHP block and advancing position by 2 characters...\n"; }
						$this->_pos += 2;
					}
					
					// We're in PHP code!
					$_in_php_section = true;
				}
				else
				{
					// We're not in a code block, keep going...
				}
			}
			else
			{
				// Are we in a function or a class?
				if($char == '{')
				{
					// if($_debug) { echo "{$this->_pos} :: Found {, Changing \$_php_level to " . ($_php_level+1) . "\n"; }
					$_php_level++;
					continue;
				}
				elseif($char == '}')
				{
					// if($_debug) { echo "{$this->_pos} :: Found }, Changing \$_php_level to = " . ($_php_level-1) . "\n"; }
					
					$_php_level--;
					if($_php_level <= 0)
					{
						// $this->_pos++;
						return;
					}
					continue;
				}
				
				// Comments can be anywhere except within a PHP statement...
				if(!$_in_php_statement)
				{
					if($_debug) { echo $this->_pos . " :: Looking for comment block...\n"; }
					
					if($char == '/')
					{
						if($nextchar == '/')
						{
							// ###
							// PHP Line Comment (like this line you're reading)
							// ###
							$comment_positions = $this->read_comment_line();
							$_currentPart = new PHP_Comment($comment_positions[0],$comment_positions[1]);
							$owner->Parts[] = $_currentPart;
							continue;
						}
						elseif($nextchar == '*')
						{
							// ###
							// PHP Comment Block (e.g. /* foo bar */)
							// ###
							$comment_positions = $this->read_comment_block();
							$_currentPart = new PHP_Comment($comment_positions[0],$comment_positions[1]);
							$owner->Parts[] = $_currentPart;
							continue;
						}
					}
				}
				
				// Not in a PHP statement, function definition, or class definition yet...
				if(!$_in_php_statement)
				{
					// Skip past any irrelevant whitespace
					if(PHP_Helpers::is_whitespace($char)) { continue; }
					
					//
					// This is the PHP block handling section (e.g. entrances into functions, classes, control structures, etc...)
					//
					
					// Look for the start of a function or class or control structure
					if($_in_PHP_Document && $this->look_ahead_for("class",$char,$nextchar))
					{
						// ###
						// PHP Class (e.g. class Foo ... )
						// ###
						// $_in_php_class = true;
						$_currentPart = new PHP_Class();
						$owner->Parts[] = $_currentPart;

						// Read class signature
						$this->read_class_signature($_currentPart);

						// Parse blass block
						$this->_Parse($_currentPart);
						continue;
					}
					elseif($_in_PHP_Document && $this->look_ahead_for("function",$char,$nextchar))
					{
						// ###
						// PHP Function (e.g. function foo($bar = "x") ... )
						// ###
						// $_in_php_function = true;
						$_currentPart = new PHP_Function();
						$owner->Parts[] = $_currentPart;
						
						// Read function signature
						$this->read_function_signature($_currentPart);
						
						// Parse function block
						$this->_Parse($_currentPart);
						// echo "Finished with " . $_currentPart->ID . ", current char = " . $this->_src[$this->_pos] . "\n";
						continue;
					}
					elseif($_in_php_class)
					{
						// Classes have some different structure - we can look for modifiers like public, protected, private, and static, as well as functions, and any variables discovered are considered properties
						
						// Check for modifiers
						if($this->look_ahead_for("public",$char,$nextchar))
						{
							$modifiers[] = "public";
							if($_modifier_start === null) { $_modifier_start = $this->_pos; }
							$this->_pos += 6;
							continue;
						}
						elseif($this->look_ahead_for("protected",$char,$nextchar))
						{
							$modifiers[] = "protected";
							if($_modifier_start === null) { $_modifier_start = $this->_pos; }
							$this->_pos += 9;
							continue;
						}
						elseif($this->look_ahead_for("private",$char,$nextchar))
						{
							$modifiers[] = "private";
							if($_modifier_start === null) { $_modifier_start = $this->_pos; }
							$this->_pos += 7;
							continue;
						}
						elseif($this->look_ahead_for("static",$char,$nextchar))
						{
							$modifiers[] = "static";
							if($_modifier_start === null) { $_modifier_start = $this->_pos; }
							$this->_pos += 6;
							continue;
						}
						elseif($this->look_ahead_for("function",$char,$nextchar))
						{
							// ###
							// PHP Class Method (e.g. public function foo($bar = "x") ... )
							// ###
							$_currentPart = new PHP_Class_Method();
							$owner->Parts[] = $_currentPart;
							
							// Read function signature
							$this->read_function_signature($_currentPart, $modifiers);
							
							// Parse function block
							$this->_Parse($_currentPart);
							
							// Finished - reset modifiers
							$modifiers = array();
							$_modifier_start == null;
							
							// ...and move on!
							continue;
						}
						elseif($char == '$')
						{
							// Class property
							$offset = PHP_Helpers::find_next(';',$this->_pos,$this->_src);
							$owner->Parts[] = new PHP_Class_Property((count($modifiers) ? $_modifier_start : $offset[0]),$offset[1] + 1,$modifiers);

							// Finished - reset modifiers
							$modifiers = array();
							$_modifier_start = null;
							continue;
						}
					}
					elseif($this->look_ahead_for("do",$char,$nextchar))
					{
						// ###
						// PHP do...while structure
						// ###
						$_currentPart = new PHP_Control_Block("do..while");
						$owner->Parts[] = $_currentPart;

						// Skip structure name
						$this->_pos += 2;
						if($_debug) { echo "{$this->_pos} :: After skipping ahead past control structure name: " . $this->show_char_context() . "\n"; }

						// Parse contents
						$this->_Parse($_currentPart);
						
						// Find while
						PHP_Helpers::find_next('w',$this->_pos,$this->_src);

						// Read block criteria
						$this->read_control_block_criteria($_currentPart);
						PHP_Helpers::find_next(';',$this->_pos,$this->_src);
						
						// echo "Finished with " . $_currentPart->ID . ", current char (" . $this->_pos . ") = " . $this->show_char_context() . "\n";
						continue;
					}
					elseif($_in_switch)
					{
						if($this->look_ahead_for("case",$char,$nextchar))
						{
							// ###
							// PHP case statement inside switch()
							// ###							
							$_currentPart = new PHP_Control_Block("case");
							$owner->Parts[] = $_currentPart;
							
							// Skip "case"
							$this->_pos += 5;
							
							// Get value
							$offsets = PHP_Helpers::find_next(':',$this->_pos,$this->_src);
							$_currentPart->Parameters = substr($this->_src,$offsets[0],$offsets[1]-$offsets[0]);
							$_currentPart->ID = $_currentPart->Type . " " . $_currentPart->Parameters;
							
							// Parse contents
							$this->_Parse($_currentPart);
							$_currentPart->Fill($this->_src);
							continue;
						}
						elseif($this->look_ahead_for("default",$char,$nextchar))
						{
							// ###
							// PHP case statement inside switch()
							// ###							
							$_currentPart = new PHP_Control_Block("case");
							$owner->Parts[] = $_currentPart;
							
							// Skip "case"
							$this->_pos += 6;
							
							// Get value
							$offsets = PHP_Helpers::find_next(':',$this->_pos,$this->_src);
							$_currentPart->ID = $_currentPart->Type . " default";
							
							// Parse contents
							$this->_Parse($_currentPart);
							$_currentPart->Fill($this->_src);
							continue;
						}
					}
					elseif($_in_switch_case && ($this->look_ahead_for("case",$char,$nextchar)))
					{
						// Stacked case statements - e.g. case "x":
						//                                case "y":
						//                                   code...
						//                                   break;
						
						// Roll back 1 char and finish this part so the next loop can start back at the "c" of the next "case"
						$this->_pos--;
						return;
					}
					else
					{
						$found_control_structure = false;
						foreach(self::$control_structures_with_criteria as $control_structure_with_criteria)
						{
							if($this->look_ahead_for($control_structure_with_criteria,$char,$nextchar))
							{
								$found_control_structure = true;
								
								// ###
								// PHP Control Structure with Criteria (if, foreach, while, etc...)
								// ###
								$_currentPart = new PHP_Control_Block($control_structure_with_criteria);
								$owner->Parts[] = $_currentPart;
								
								// Skip structure name
								$this->_pos += strlen($control_structure_with_criteria);
								
								// $_debug = (($this->_pos > 48100) && ($this->_pos < 48250));
								// if($_debug) { echo "{$this->_pos} :: After skipping ahead past control structure name: " . $this->show_char_context() . "\n"; }
								
								// Read block criteria
								$this->read_control_block_criteria($_currentPart);
								
								// Parse contents
								$this->_Parse($_currentPart);
								// echo "Finished with " . $_currentPart->ID . ", current char (" . $this->_pos . ") = " . $this->show_char_context() . "\n";
								break;
							}
						}
						if($found_control_structure)
						{
							continue;
						}
						elseif($this->look_ahead_for("else",$char,$nextchar)) // If we find an else that isn't an elseif or else if
						{
							// ###
							// PHP else structure
							// ###
							$_currentPart = new PHP_Control_Block("else");
							$owner->Parts[] = $_currentPart;
            	
							// Skip structure name
							$this->_pos += 4;
            	
							// Parse contents
							$this->_Parse($_currentPart);
							
							continue;
						}
					}
				}
				
				// 
				// This is the PHP statement handling section
				//
				
				
				// $_debug = (($this->_pos >= 13250) && ($this->_pos < 14000));
				// if($_debug) { echo "{$this->_pos} :: Statement area, \$_in_php_statement = " . var_export($_in_php_statement,true) . ", " . $this->show_char_context() . "\n"; }
				
				// Look for end of PHP code block
				if(($char == '?') && ($nextchar == '>'))
				{
					if($_debug) { echo "{$this->_pos} :: Exiting PHP block and advancing position by 1 character...\n"; }
					
					// Exiting...
					$_in_php_section = false;
					
					// Mark the offset for the next non-codeblock-start
					$_non_codeblock_start = $this->_pos + 2;
					
					$this->_pos++;
				}
				elseif(($char == '"') || ($char == '\''))
				{
					// Entering a quoted value
					$quoted_value = PHP_Helpers::read_quoted_value($char, $this->_pos, $this->_src);
				}
				elseif($char == ';')
				{
					// ###
					// PHP Statement (e.g. $x = "foo";)
					// ###
					$_in_php_statement = false;
					if($_debug) { echo "{$this->_pos} :: Ending PHP statement: " . substr($this->_src,$_php_statement_start,5). "..." . substr($this->_src,$this->_pos - 5,6) . "\n"; }
					if(!($_currentPart instanceof PHP_StatementBlock))
					{
						$_currentPart = new PHP_StatementBlock;
						$owner->Parts[] = $_currentPart;
					}
					$_currentPart->Add($_php_statement_start, $this->_pos + 1);
					
					// If we're in a switch -> case block and we've found a break; on level 0, then return
					if($_in_switch_case && ($_php_level == 0))
					{
						$statement = substr($this->_code,$_php_statement_start, ($this->_pos - $_php_statement_start));
						if($statement == "break")
						{
							return;
						}
					}
					// If we've just found a PHP statement while inside of a control block like if() but we're at level 0, then we're dealing
					// with shorthand (no curly braces) and need to return after this statement.
					elseif(($owner instanceof PHP_Control_Block) && ($_php_level == 0))
					{
						return;
					}
				}
				elseif($_in_php_statement && ($char == '<') && ($nextchar == '<') && (substr($this->_src,$this->_pos+2,1) == '<'))
				{
					// Start of Heredoc assignment
					$this->_pos += 3;
					$offsets = PHP_Helpers::find_next("\n",$this->_pos,$this->_src);
					$heredoc_end = "\n" . trim(substr($this->_src,$offsets[0],($offsets[1]-$offsets[0]))) . ';';
					$heredoc_end_len = strlen($heredoc_end);
					
					// Look for ending heredoc line and skip to the final character, immediately before the ;
					$heredoc_len = strpos(substr($this->_src,$this->_pos), $heredoc_end);
					$this->_pos += $heredoc_len + $heredoc_end_len - 2;
					
					// Now continue the loop and let the code handle the end of the statement
					if($_debug) { echo "{$this->_pos} :: Heredoc End :: " . $this->show_char_context() . "\n"; }
					continue;
				}
				elseif($_in_php_statement && ((($char == '/') && ($nextchar == '/')) || (($char == '/') && ($nextchar == '/'))))
				{
					// Handle situations where comments are in the middle of a multi-line piece of code, like:
					// $x = array(
					//   "foo", // A comment
					//   "bar"  /* a big comment */
					// );
					
					if($nextchar == '/')
					{
						// ###
						// PHP Line Comment (like this line you're reading)
						// ###
						$comment_positions = $this->read_comment_line();
						$owner->Parts[] = new PHP_Comment($comment_positions[0],$comment_positions[1]);
						continue;
					}
					elseif($nextchar == '*')
					{
						// ###
						// PHP Comment Block (e.g. /* foo bar */)
						// ###
						$comment_positions = $this->read_comment_block();
						$owner->Parts[] = new PHP_Comment($comment_positions[0],$comment_positions[1]);
						continue;
					}
				}
				elseif(!$_in_php_statement)
				{
					if($_debug) { echo "{$this->_pos} :: Starting PHP statement: " . $this->show_char_context() . "\n"; }
					
					// Beginning of a PHP statement
					$_in_php_statement = true;
					$_php_statement_start = $this->_pos;
				}
				
				// if($_debug) { echo "{$this->_pos} :: End of statement area, \$_in_php_statement = " . var_export($_in_php_statement,true) . "\n"; }
			}
		}
		
		// Reached the end - do we need to wrap up the non-codeblock?
		if(!$_in_php_section)
		{
			// We're entering PHP, so add any non-codeblock contents we've accumulated so far
			if($this->_pos > $_non_codeblock_start)
			{
				// ###
				// PHP Non-Code Block (e.g. HTML)
				// ###
				$_currentPart = new PHP_NonCodeBlock($_non_codeblock_start, $this->_pos);
				$owner->Parts[] = $_currentPart;
			}
		}
		else
		{
			// PHP code continues to the end
		}
	}
	
	// ###########################################################################
	// Helper functions
	// ###########################################################################

	// Debugging helper
	private function show_char_context($pos = null, $numContext = 10)
	{
		if($pos === null) { $pos = $this->_pos; }
		return PHP_Helpers::show_char_context($pos, $this->_src, $numContext);
	}
	
	// Look ahead in the code for specific keywords
	private function look_ahead_for($word, $char, $nextchar)
	{
		// Check first 2 chars to quickly eliminate false matches
		if(($word[0] != $char) && ($word[1] != $nextchar)) { return false; }
		
		// Check for the full word match
		$len = strlen($word);
		if(substr($this->_code,$this->_pos,$len) != $word) { return false; }
		
		// Check the character after the full word match to make sure it's a boundary character
		if(PHP_Helpers::is_valid_name_char($this->_code[$this->_pos + $len])) { return false; }
		
		// We've got a match!
		return true;
	}
	
	// Read comment line
	private function read_comment_line()
	{
		$start = $this->_pos;

		for($this->_pos = ($start); $this->_pos < $this->_end; $this->_pos++)
		{
			// Move pointer forward as we parse
			$char = $this->_code[$this->_pos];
			
			if($char == "\n")
			{
				// End of the line
				return array($start,$this->_pos + 1);
			}
		}
	}

	// Read comment block
	private function read_comment_block()
	{
		$start = $this->_pos;
		$lastchar = null;
		
		for($this->_pos = ($start + 1); $this->_pos < $this->_end; $this->_pos++)
		{
			// Move pointer forward as we parse
			$char = $this->_code[$this->_pos];
			
			if(($char == '/') && ($lastchar == '*'))
			{
				// End of the comment block
				return array($start, $this->_pos + 1);
			}
			
			// Track the last char so we can check for */
			$lastchar = $char;
		}
	}
	
	// Parse the function signature / definition (Name and Parameters)
	private function read_function_signature(PHP_Function $Function,$Modifiers = null)
	{
		// At this point, the current char will be at the starting "f" of "function"
		
		// Skip to function name
		PHP_Helpers::fast_forward($this->_pos,$this->_src);
		
		// Now skip to start of parameters (beginning parentheses)
		$offsets = PHP_Helpers::fast_forward($this->_pos,$this->_src,'(');
		$Function->Name = substr($this->_src,$offsets[0],($offsets[1]-$offsets[0]));
		$Function->ID = $Function->Name;
		
		// Capture any parameters
		$this->_pos++;
		$offsets = PHP_Helpers::find_matching_close_char('(', $this->_pos, $this->_src);
		$Function->Parameters = substr($this->_src,$offsets[0],($offsets[1]-$offsets[0]));
		
		// For class methods, add any modifiers like scope or static
		if(is_array($Modifiers) && count($Modifiers))
		{
			$Function->Modifiers = $Modifiers;
		}
	}
	
	// Parse the class signature / definition (Name)
	private function read_class_signature(PHP_Class $Class)
	{
		// At this point, the current char will be at the starting "c" of "class"
		
		// Skip to class name
		PHP_Helpers::fast_forward($this->_pos,$this->_src);
		
		// Skip to the starting brace, and trim what we've passed as the class name		
		$offsets = PHP_Helpers::fast_forward($this->_pos,$this->_src, '{');
		$Class->Name = trim(substr($this->_src,$offsets[0],($offsets[1]-$offsets[0])));
	}

	// Read the parameters for a control block that uses them - e.g. if(foo == bar)
	private function read_control_block_criteria(PHP_Control_Block $Block)
	{
		if(($Block->Type == "if") || ($Block->Type == "foreach") || ($Block->Type == "for") || ($Block->Type == "while")  || ($Block->Type == "else if")  || ($Block->Type == "elseif") || ($Block->Type == "switch") || ($Block->Type == "do..while"))
		{
			// Find start of criteria block
			PHP_Helpers::fast_forward($this->_pos,$this->_src, '(');

			// $_debug = (($this->_pos > 48100) && ($this->_pos < 48250));
			// if($_debug) { echo "{$this->_pos} :: After fast_forward: " . $this->show_char_context() . "\n"; }
			
			$this->_pos++;
			$offsets = PHP_Helpers::find_matching_close_char('(', $this->_pos, $this->_src);
			
			$Block->Parameters = substr($this->_src,$offsets[0],($offsets[1]-$offsets[0]));
			$Block->ID = $Block->Type . " (" . $Block->Parameters . ")";
		}
	}
}
