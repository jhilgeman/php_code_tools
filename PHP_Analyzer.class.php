<?php
/* PHP Analyzer v0.1
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * This tool will take the results of a parsed PHP file and compiled various
 * statistics about the code.
 */

namespace PHP_Code_Tools;

class PHP_Analyzer
{
	private static $internal_functions = null;

	// After parsing, analyze the results
	public static function Analyze(PHP_Document $Results)
	{
		$stats = array
		(
			"namespace" => "",
			"functions_called" => array(),
			"functions_defined" => array(),
			"classes_defined" => array(),
			"possible_sql_queries" => array(),
			"included_or_required_files" => array()
		);
		
		self::AnalyzePart($Results, $stats);
		
		// Remove various functions from the functions_called
		if(self::$internal_functions === null)
		{
			$defined_functions = get_defined_functions();
			self::$internal_functions = $defined_functions["internal"];
			self::$internal_functions[] = "include";
			self::$internal_functions[] = "include_once";
			self::$internal_functions[] = "require";
			self::$internal_functions[] = "require_once";
		}
		$stats["functions_called"] = array_diff($stats["functions_called"], self::$internal_functions);
		
		return $stats;
	}
	
	public static function AnalyzePart(PHP_Code_Container $Container, &$stats)
	{
		foreach($Container->Parts as $Part)
		{
			if($Part instanceof PHP_StatementBlock)
			{
				foreach($Part->Statements as $phpStatement)
				{
					$phpStatement = $phpStatement[2];
					
					// Analyze the PHP statement
					self::_AnalyzePHPStatement($phpStatement, $stats);
				}
			}
			elseif($Part instanceof PHP_Function)
			{
				if(!($Part instanceof PHP_Class_Method))
				{
					$stats["functions_defined"][] = $Part->Name;
				}
			}
			elseif($Part instanceof PHP_Class)
			{
				$stats["classes_defined"][] = $Part->Name;
			}
			
			// Recurse into sub-parts
			if($Part instanceof PHP_Code_Container)
			{
				self::AnalyzePart($Part,$stats);
			}
		}
	}
	
	private static function _AnalyzePHPStatement($phpStatement, &$stats)
	{
		// Get all quoted values
		
		// Look for possible function calls
		if(strpos($phpStatement,"("))
		{
			if(preg_match_all("@([A-Za-z0-9_\\\]+)\s*\(@",$phpStatement,$matches))
			{
				$stats["functions_called"] = array_unique(array_merge($stats["functions_called"], $matches[1]));
			}

			// Look for require/include
			if(preg_match("@(?:include|require)(?:_once)?\s*\(?([^\);]+)\)?@",$phpStatement,$matches))
			{
				$stats["included_or_required_files"][] = $matches[1];
			}

		}
		
		// Report lines that sound like they might have or build queries
		if(stripos($phpStatement,"UPDATE") && stripos($phpStatement,"SET") && preg_match("@UPDATE\s@i",$phpStatement) && preg_match("@\sSET\s@i",$phpStatement))
		{
			if(preg_match_all("@(?:query\s*\(|=\s*|\()[\"']\s*(UPDATE\b.+\bSET\b.+[^;]+;)@is",$phpStatement,$matches))
			{
				foreach($matches[1] as $query)
				{
					$stats["possible_sql_queries"][] = $query;
				}
			}
			else
			{
				$stats["possible_sql_queries"][] = $phpStatement;
			}
		}
		elseif(stripos($phpStatement,"DELETE FROM"))
		{
			if(preg_match_all("@(?:query\s*\(|=\s*|\()[\"']\s*(DELETE\s+FROM\b.+[^;]+;)@is",$phpStatement,$matches))
			{
				foreach($matches[1] as $query)
				{
					$stats["possible_sql_queries"][] = $query;
				}
			}
			else
			{
				$stats["possible_sql_queries"][] = $phpStatement;
			}
		}
		elseif(stripos($phpStatement,"INSERT INTO"))
		{
			if(preg_match_all("@(?:query\s*\(|=\s*|\()[\"']\s*(INSERT\s+INTO\b.+[^;]+;)@is",$phpStatement,$matches))
			{
				foreach($matches[1] as $query)
				{
					$stats["possible_sql_queries"][] = $query;
				}
			}
			else
			{
				$stats["possible_sql_queries"][] = $phpStatement;
			}
		}
		elseif(stripos(str_ireplace("<SELECT","",$phpStatement),"SELECT") && preg_match("@\sFROM\s@i",$phpStatement))
		{
			if(preg_match_all("@(?:query\s*\(|=\s*|\()[\"']\s*(SELECT\b.+\bFROM\b.+[^;]+;)@is",$phpStatement,$matches))
			{
				foreach($matches[1] as $query)
				{
					$stats["possible_sql_queries"][] = $query;
				}
			}
			else
			{
				$stats["possible_sql_queries"][] = $phpStatement;
				echo __LINE__ . " :: " . print_r($phpStatement,true); die();
			}
		}
		
	}
}
