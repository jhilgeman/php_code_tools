<?php
/* PHP Parser Types
 * @author: Jonathan Hilgeman (contact at sitecreative dot com)
 * @updated: 2015-10-30
 * @package: PHP_Code_Tools
 *
 * These are the various supporting types used by the PHP_Code_Tools package.
 */

namespace PHP_Code_Tools;

/* PHP_Code_Containers can be passed to the Parse function so they hold the parsed contents. */
class PHP_Code_Container
{
	public $ID = "";
	public $Parts = array();

	public function Fill(&$code)
	{
		foreach($this->Parts as $Part)
		{
			$Part->Fill($code);
		}
	}
}

class PHP_Document extends PHP_Code_Container
{
	public $ID = "PHP Document";
}

class PHP_Control_Block extends PHP_Code_Container
{
	public $Type;
	public $Parameters;
	public function __construct($Type)
	{
		$this->Type = $Type;
		$this->ID = $Type;
	}
}

class PHP_Function extends PHP_Code_Container
{
  public $Name;
  public $Parameters;
	public $Start;
	public $End;
}

class PHP_Class extends PHP_Code_Container
{
  public $Name;
  public $Namespace;
	public $Start;
	public $End;
}

class PHP_Class_Method extends PHP_Function
{
  public $Name;
  public $Modifiers;
  public $Parameters;
	public $Start;
	public $End;
}


/* Regular containers for various code statemnents */
class PHP_Basic_Fillable
{
	public $Start;
	public $End;
	public $Contents;
	
	public function __construct($Start,$End)
	{
		$this->Start = $Start;
		$this->End = $End;
	}

	public function Fill(&$code)
	{
		if($this->Contents == null)
		{
			$this->Contents = substr($code,$this->Start,($this->End-$this->Start));
		}
	}
}

class PHP_StatementBlock
{
	public $Statements = array();
	public function Add($Start,$End)
	{
		$this->Statements[] = array($Start,$End);
	}
	
	public function Fill(&$code)
	{
		foreach($this->Statements as $idx => $arrPositions)
		{
			if(!isset($arrPositions[2]))
			{
				$start = $arrPositions[0];
				$end = $arrPositions[1];
				$this->Statements[$idx][2] = substr($code,$start,($end-$start));
			}
		}
	}
}

class PHP_NonCodeBlock extends PHP_Basic_Fillable
{
}

class PHP_Comment extends PHP_Basic_Fillable
{
}

class PHP_Class_Property extends PHP_Basic_Fillable
{
	public $Modifiers = array();
	public function __construct($Start,$End,$Modifiers = array())
	{
		$this->Start = $Start;
		$this->End = $End;
		$this->Modifiers = $Modifiers;
	}
}